#coding: utf-8
from builtins import input
import logging
import getpass
import pickle

import requests
from lxml import html


INCORRECT_USERNAME_PASSWORD = "Le nom d'utilisateur ou le mot de passe est incorrect."

class DokuwikiAPIError(Exception):
    pass
class LoginError(DokuwikiAPIError):
    pass
class PageAlreadyExistsError(DokuwikiAPIError):
    pass
class AccessDeniedError(DokuwikiAPIError):
    pass
class PageCreationError(DokuwikiAPIError):
    pass

logging.getLogger('requests').setLevel(logging.WARNING)
logging.getLogger('urllib3').setLevel(logging.WARNING)

class DokuwikiAPI(object):

    def __init__(self, wiki_url, base_path='', cookiejar=None):
        self.logger = logging.getLogger(__name__)
        if wiki_url[-1] != '/':
            wiki_url += '/'
        self.wiki_url = wiki_url

        base_path = self.sanitize_path(base_path)
        self.base_path = base_path + '/'
        self.session = requests.session()

        if cookiejar is not None:
            self.cookies = cookiejar

        self.logger.debug('Wiki url set to  : {}.'.format(self.wiki_url))
        self.logger.debug('Base path set to : {}.'.format(self.base_path))

    def __del__(self):
        self.session.close()
        self.logger.debug('Closing session.')

    def ask_credentials(self):
        user = input('User: ')
        password = getpass.getpass(prompt='Password: ')
        return user, password

    def login(self, prompt=False, user=None, password=None):
        if prompt:
            user, password = self.ask_credentials()
        data = {
            'u': user,
            'p': password,
        }
        self.logger.debug('Loging in at page {}.'.format(self.wiki_url))
        response = self.session.post(self.wiki_url, data=data)
        tree = html.fromstring(response.text)

        error_messages = tree.xpath('//div[@class="error"]/text()')
        for error_message in error_messages:
            if INCORRECT_USERNAME_PASSWORD in error_message:
                error_message = 'Incorrect login or password'
                self.logger.error('Could not login : {}.'.format(error_message))
                raise LoginError(error_message)

        if not self.is_logged(response):
            error_message = 'Unknown login error'
            self.logger.error('Could not login : {}.'.format(error_message))
            raise LoginError(error_message)

        self.logger.info('Logged in.')

    def is_logged(self, response=None):
        if response is None:
            response = self.session.post(self.wiki_url)
        tree = html.fromstring(response.text)
        return not tree.xpath('//a[@class="action btn btn-default navbar-btn login"]')

    def page_exists(self, response=None):
        tree = html.fromstring(response.text)
        if not self.has_access_to(response=response):
            raise AccessDeniedError('Access to page denied.')
        return not tree.xpath('//a[@class="action create"]')

    def has_access_to(self, response=None):
        tree = html.fromstring(response.text)
        return not tree.xpath('//h1[@id="autorisation_refusee"]')

    @property
    def cookies(self):
        return self.session.cookies
    @cookies.setter
    def cookies(self, cookiejar):
        self.session.cookies = cookiejar

    def load_cookies_from_pickle(self, path):
        try:
            with open(path, 'rb') as cookie_p:
                cookies = pickle.load(cookie_p)
        except IOError as e:
            self.logger.error('Could not load cookies from pickle : {}.'.format(e))
            raise
        else:
            self.logger.info('Loaded cookies from {}.'.format(path))
            self.cookies = cookies

    def can_edit(self, response):
        tree = html.fromstring(response.text)
        return tree.xpath('//a[@class="action edit"]') or tree.xpath('//a[@class="action create"]')

    @staticmethod
    def sanitize_path(path):
        path = path.replace(':', '/')
        while path[0] == '/':
            path = path[1:]
        while path[-1] == '/':
            path = path[:-1]
        return path

    def path2url(self, path):
        return self.wiki_url + self.base_path + self.sanitize_path(path)

    def get_response(self, path):
        url = self.path2url(path)
        response = self.session.get(url)
        return response

    def create_page(self, page_path, content=''):
        url = self.path2url(page_path)
        self.logger.info('Creating page {}.'.format(url))
        response = self.session.get(url)

        if not self.has_access_to(response):
            error_message = 'Access to page {} denied'.format(url)
            self.logger.error(error_message)
            raise AccessDeniedError(error_message)
        if self.page_exists(response):
            error_message = 'Page {} already exists.'.format(url)
            self.logger.error(error_message)
            raise PageAlreadyExistsError(error_message)
        if not self.can_edit(response):
            error_message = 'Page {} is read only.'.format(url)
            self.logger.error(error_message)
            raise AccessDeniedError(error_message)

        response = self.session.get(url + '?do=edit')

        tree = html.fromstring(response.text)

        sectok = tree.xpath('//input[@name="sectok"]/@value')[0]
        changecheck = tree.xpath('//input[@name="changecheck"]/@value')[0]
        id = tree.xpath('//input[@name="id"]/@value')[0]
        target = tree.xpath('//input[@name="target"]/@value')[0]

        data = {
            'do': 'edit',
            'changecheck': changecheck,
            'date': '',
            'do[save]': '',
            'prefix': '.',
            'rev': '0',
            'sectok': sectok,
            'suffix': '',
            'summary': 'créée depuis DokuwikiAPI',
            'target': target,
            'id': id,
            'wikitext': content,
        }
        response = self.session.post(url, data=data)
        if not self.page_exists(response):
            with open('response.html', 'w') as f:
                f.write(response.text)
            self.logger.error('The page {} has not been created. The response has been saved at response.html .'.format(url))
            raise PageCreationError('Unknown error during page creation.')



