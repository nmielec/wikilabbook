""" Wikilabbook functions returning labbook pages for the day, month and year
"""
import datetime
import calendar

from . import config

def get_day_page(year, month, day):
    """ returns the page of the requested day """
    day_dt = datetime.datetime(year, month, day)

    nextday_dt = day_dt + datetime.timedelta(days=1)
    prevday_dt = day_dt - datetime.timedelta(days=1)

    if config.TEMPLATE['REMOVE_WEEKENDS']:
        if day_dt.isoweekday() == 1:
            prevday_dt = day_dt - datetime.timedelta(days=3)
        if day_dt.isoweekday() == 5:
            nextday_dt = day_dt + datetime.timedelta(days=3)


    return config.TEMPLATE['DAY'].format(
        labbook_path=config.TEMPLATE['LABBOOK_PATH'],
        day=day_dt,
        prevday=prevday_dt,
        nextday=nextday_dt,
        )

def get_year_page(year):
    """ returns the page of the requested year """
    lines = [config.TEMPLATE['YEAR_HEADER'].format(year=year)]
    for monthnum in range(1, 13):
        month_dt = datetime.datetime(year, monthnum, 1)
        lines.append(
            config.TEMPLATE['YEAR_MONTH'].format(
                labbook_path=config.TEMPLATE['LABBOOK_PATH'],
                year=year,
                month=month_dt
                )
            )
    return '\n'.join(lines)

def get_month_page(year, month):
    """ returns the page of the requested month """
    month_dt = datetime.datetime(year, month, 1)
    numdays = calendar.monthrange(year=year, month=month)[1]
    lastday = datetime.datetime(year=year, month=month, day=numdays)
    firstday = datetime.datetime(year=year, month=month, day=1)
    prevmonth_dt = firstday - datetime.timedelta(days=1)
    nextmonth_dt = lastday + datetime.timedelta(days=1)

    formatters = dict(
        labbook_path=config.TEMPLATE['LABBOOK_PATH'],
        month=month_dt,
        prevmonth=prevmonth_dt,
        nextmonth=nextmonth_dt,
    )

    lines = [config.TEMPLATE['MONTH_HEADER'].format(**formatters)]

    for daynum in range(1, numdays+1):
        day_dt = datetime.datetime(year=month_dt.year, month=month_dt.month, day=daynum)
        if config.TEMPLATE['REMOVE_WEEKENDS'] and day_dt.isoweekday() in (6, 7):
            continue
        lines.append(
            config.TEMPLATE['MONTH_DAY'].format(
                DayPadded='{:10s}'.format(day_dt.strftime('%A')),
                day=day_dt,
                **formatters
                )
            )

    lines.append(config.TEMPLATE['MONTH_FOOTER'].format(**formatters))

    return '\n'.join(lines)
